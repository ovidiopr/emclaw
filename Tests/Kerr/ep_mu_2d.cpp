//
// Created by ovidio on 11/11/20.
//

#include "DEFINES.H"
#include <AMReX_FArrayBox.H>

// ep,mu
void get_ep_mu(const amrex_real *prob_lo, amrex::Box const &bx,
               amrex::FArrayBox &uout, amrex::FArrayBox &ep, amrex::FArrayBox &mu,
               const amrex_real *dx, const int &nGhost,
               const int &comp, const int &Dwave, const amrex_real &time) {

    const auto lo = amrex::lbound(bx);
    const auto hi = amrex::ubound(bx);

    int i, j;
    double x, y, r2;

    ep = 1.0;
    mu = 1.0;

    amrex::Array4<amrex::Real> const &ep_a = ep.array();
    amrex::Array4<amrex::Real> const &mu_a = mu.array();
    amrex::Array4<amrex::Real> const &uout_a = uout.array();

    for (int j = lo.y; j <= hi.y; ++j) {
        y = prob_lo[1] + (j + 0.5)*dx[1];
        for (int i = lo.x; i <= hi.x; ++i) {
            x = prob_lo[0] + (i + 0.5)*dx[0];
//                    ep_a(i, j, 0) = ...
//                    CIRCLE_2D(0.0,0.0,0.25,4.0d0,1.0d0)
        }
    }

    // Store in StateData
    for (int j = lo.y; j <= hi.y; ++j) {
        for (int i = lo.x; i <= hi.x; ++i) {
            if (Dwave == 1) {
                uout_a(i, j, 0, 4) = ep_a(i, j, 0, 0);
                uout_a(i, j, 0, 5) = ep_a(i, j, 0, 1);
                uout_a(i, j, 0, 6) = mu_a(i, j, 0, 2);
            } else {
                uout_a(i, j, 0, 4) = mu_a(i, j, 0, 0);
                uout_a(i, j, 0, 5) = mu_a(i, j, 0, 1);
                uout_a(i, j, 0, 6) = ep_a(i, j, 0, 2);
            }
        }
    }


}
