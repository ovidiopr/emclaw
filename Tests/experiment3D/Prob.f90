subroutine amrex_probinit (init, name, namlen, problo, probhi) bind(c)

    implicit none

    integer, intent(in) :: init, namlen
    integer, intent(in) :: name(namlen)
    double precision, intent(in) :: problo(*), probhi(*)

    integer untin, i

end subroutine amrex_probinit


subroutine initdata(level, time, lo, hi, &
        em, em_lo, em_hi, &
        dx, prob_lo, nComp) bind(C, name = "initdata")

    implicit none
    integer, intent(in) :: level, lo(3), hi(3), em_lo(3), em_hi(3), nComp
    double precision, intent(in) :: time
    double precision, intent(inout) :: em(em_lo(1):em_hi(1), &
            &                                 em_lo(2):em_hi(2), &
            &                                 em_lo(3):em_hi(3), 0:nComp)
    double precision, intent(in) :: dx(3), prob_lo(3)

    integer :: dm
    integer :: i, j, k
    double precision :: x, y, z, r2

    if (em_lo(3) .eq. 0 .and. em_hi(3) .eq. 0) then
        dm = 2
        if (em_lo(2) .eq. 0 .and. em_hi(2) .eq. 0) then
            dm = 1
        end if
    else
        dm = 3
    end if

    do k = lo(3), hi(3)
        z = prob_lo(3) + (dble(k) + 0.5d0) * dx(3)
        do j = lo(2), hi(2)
            y = prob_lo(2) + (dble(j) + 0.5d0) * dx(2)
            do i = lo(1), hi(1)
                x = prob_lo(1) + (dble(i) + 0.5d0) * dx(1)
                if (dm.eq. 3) then
                   if ((z.ge.0.085e-6).and.(z.le.0.635e-6).and.(y.ge.0.095e-6).and.(y.le.0.325e-6) &
     &                 .and.(x.ge.0.11e-6).and.(x.le.0.61e-6)) then
                      em(i,j,k,29) = 1e16;
                   endif
                   if ((z.ge.0.085e-6).and.(z.le.0.635e-6).and.(y.ge.0.41e-6).and.(y.le.0.61e-6) &
     &                 .and.(x.ge.0.11e-6).and.(x.le.0.61e-6)) then
                      em(i,j,k,33) = 1e16;
                   endif
                endif

            end do
        end do
    end do

end subroutine initdata
