#!/usr/bin/env python3 
import numpy as np
from scipy.integrate import odeint
#import matplotlib
import matplotlib.pyplot as plt
# dielectric function
def dielectric(z,u):
    EPSILON1=u[5]
    ZLEFT   =u[7]
    ZRIGHT  =u[8]
    if z<0:
        if z> ZLEFT:
            g=EPSILON1-(1-EPSILON1)*z
            gp=-(1-EPSILON1)
        else:
            g=EPSILON1-(1-EPSILON1)*ZLEFT
            gp=0
    else:
        if z<ZRIGHT:
            g=EPSILON1+(1-EPSILON1)*z
            gp=(1-EPSILON1)
        else:
            g=EPSILON1-(1-EPSILON1)*ZRIGHT
            gp=0
    return (g,gp)
def ginzburg(y,z,u):
    # model con z <--> -z
    y0 = y[0]
    y1 = y[1]
    dy0dz = -y1
    POLARIZATION=u[2]
    OMEGAC2     =u[0]
    ALPHA2      =u[1]
    # debiera ponerse z<epsilon1, pero EMCLAW pone CC abs. en z=0
    (g,gp)=dielectric(z,u)
    dy1dz = POLARIZATION*gp*y1/g+OMEGAC2*(g-EPSILON3*ALPHA2)*y0
    dydz = [dy0dz,dy1dz]
    return dydz   
# Max for drawing
def maxpos(z,f,a):
    fmax=f[0]
    for  i in range(z.size):
        if z[i]>a:
            if f[i] > fmax:
                fmax=f[i]
    return fmax
# number of space points
n =50000
n2=int(n/2)
# omega/c
omegac=20*np.pi
# slope of epsilon
a=0.4
# initial condition
u=np.zeros(10)
u[0] = OMEGAC2=(omegac/a)**2 # omega^2/c^2/a^2
u[1] = ALPHA2 =0.1           # alpha_0^2
u[2] = POLARIZATION  =1      # 1=p polarization, 0=s polarization
u[3] = EPSILON1 = 0.001      # region 1 = epsilon_1 (a=1)
u[4] = EPSILON3 = 1.0        # region 2 = epsilon_3 (a=1)
u[5] = u[3]                  # epsilon_1
u[6] = u[4]                  # epsilon_3
u[7] = ZLEFT = -1            # Left z
u[8] = ZRIGHT =2             # Right z
# z normalized to z -> a z, so a_new=1
az = np.linspace(ZLEFT,ZRIGHT,n)
z = -az/a
# store solution
Hx = np.empty_like(az)
DHx= np.empty_like(az)
Ez = np.empty_like(az)
Ey = np.empty_like(az)
Ez2= np.empty_like(az)
# record initial conditions
k = np.sqrt(np.abs(OMEGAC2*(EPSILON1-EPSILON3*ALPHA2)))
k = np.sqrt(np.abs(OMEGAC2*EPSILON1))
# initial field values
Hx [0] = 1
DHx[0] = k*Hx[0]
# solve ODE
for i in range(1,n):
    # solve 
    oderesult = odeint(ginzburg,[Hx[i-1],DHx[i-1]],[az[i-1],az[i]],args=(u,))
    # store solution for plotting
    Hx [i] = oderesult[1][0]
    DHx[i] = oderesult[1][1]
    Ez2[i] = (2*Hx [i]/(z[i-1]+z[i]))**2
    Ey[i] = 2*DHx[i]/(z[i-1]+z[i])
Emax=maxpos(az,Ez2,ALPHA2)
Hxmax=maxpos(az,Hx,ALPHA2)
Eymax=maxpos(az,Ey,ALPHA2)
# plot results compared with emclaw
plt.plot(z,Ez2,'b-',label='Ez^2')
plt.ylabel('Ez^2')
plt.xlabel('z')
plt.ylim(0,Emax)
plt.xlim(ZLEFT,0.1)
plt.legend(loc='best')
plt.savefig("Ez2CompareRamp.svg", format="svg")
plt.show()
