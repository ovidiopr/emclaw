// license_AMReX.txt
// license_EMcLAW.txt
#include <EM.H>
#include <EM_F.H>
#include <AMReX_VisMF.H>
#include <AMReX_TagBox.H>
#include <AMReX_ParmParse.H>

#include "DEFINES.H"

using namespace amrex;

int      EM::verbose = 0;
Real     EM::cfl = 0.4;
int      EM::do_reflux = 1;
int      EM::is_D_wave = 1; // D-wave or B-wave

int      EM::NUM_STATE = 8;
int      EM::NUM_STATE_U = 8;
int      EM::NUM_GROW = 4;  // number of ghost cells

SOURCES::C_sources CV_sources1;
SOURCES::C_sources EM::f_sources = CV_sources1;

POL::C_polarizations CV_pol1;
POL::C_polarizations EM::Polarizations = CV_pol1;

#ifdef PARTICLES
std::unique_ptr<AmrTracerParticleContainer> EM::TracerPC =  nullptr;
int EM::do_tracers                       =  0;
#endif

//
//Default constructor.  Builds invalid object.
//
EM::EM() {
    flux_reg = 0;
}

//
//The basic constructor.
//
EM::EM(Amr &papa,
       int lev,
       const Geometry &level_geom,
       const BoxArray &bl,
       const DistributionMapping &dm,
       Real time)
        :
        AmrLevel(papa, lev, level_geom, bl, dm, time) {
    flux_reg = 0;
    if (level > 0 && do_reflux)
        flux_reg = new FluxRegister(grids, dmap, crse_ratio, level, NUM_STATE);
}

//
//The destructor.
//
EM::~EM() {
    delete flux_reg;
}

//
//Restart from a checkpoint file.
//
void EM::restart(Amr &papa, std::istream &is, bool bReadSpecial) {
    AmrLevel::restart(papa, is, bReadSpecial);

    BL_ASSERT(flux_reg == 0);
    if (level > 0 && do_reflux)
        flux_reg = new FluxRegister(grids, dmap, crse_ratio, level, NUM_STATE);
}

void EM::checkPoint(const std::string &dir,
                    std::ostream &os,
                    VisMF::How how,
                    bool dump_old) {
    AmrLevel::checkPoint(dir, os, how, dump_old);
#ifdef PARTICLES
    if (do_tracers and level == 0) {
      TracerPC->Checkpoint(dir, "Tracer", true);
    }
#endif
}

//
//Initialize data on this level from another EM AmrLevel (during regrid).
//
void EM::init(AmrLevel &old) {
    EM *oldlev = (EM *) &old;
    //
    // Create new grid data by fillpatching from old.
    //
    Real dt_new = parent->dtLevel(level);
    Real cur_time = oldlev->state[State_Type].curTime();
    Real prev_time = oldlev->state[State_Type].prevTime();
    Real dt_old = cur_time - prev_time;
    setTimeLevel(cur_time, dt_old, dt_new);

    MultiFab &S_new = get_new_data(State_Type);

    FillPatch(old, S_new, 0, cur_time, State_Type, 0, NUM_STATE);
}

//
//Initialize data on this level after regridding if old level did not previously exist
//
void EM::init() {
    Real dt = parent->dtLevel(level);
    Real cur_time = getLevel(level - 1).state[State_Type].curTime();
    Real prev_time = getLevel(level - 1).state[State_Type].prevTime();

    Real dt_old = (cur_time - prev_time) / (Real) parent->MaxRefRatio(level - 1);

    setTimeLevel(cur_time, dt_old, dt);
    MultiFab &S_new = get_new_data(State_Type);
    FillCoarsePatch(S_new, 0, cur_time, State_Type, 0, NUM_STATE);
}

//
//Do work after timestep().
//
void EM::post_timestep(int iteration) {
    //
    // Integration cycle on fine level grids is complete
    // do post_timestep stuff here.
    //
    int finest_level = parent->finestLevel();

    if (do_reflux && level < finest_level)
        reflux();

    if (level < finest_level)
        avgDown();

#ifdef PARTICLES
    if (TracerPC)
      {
        const int ncycle = parent->nCycle(level);

        if (iteration < ncycle || level == 0)
      {
            int ngrow = (level == 0) ? 0 : iteration;

        TracerPC->Redistribute(level, TracerPC->finestLevel(), ngrow);
      }
      }
#endif
}

//
//Do work after regrid().
//
void EM::post_regrid(int lbase, int new_finest) {
#ifdef PARTICLES
    if (TracerPC && level == lbase) {
        TracerPC->Redistribute(lbase);
    }
#endif
}

//
//Do work after a restart().
//
void EM::post_restart() {
#ifdef PARTICLES
    if (do_tracers and level == 0) {
      BL_ASSERT(TracerPC == 0);
      TracerPC.reset(new AmrTracerParticleContainer(parent));
      TracerPC->Restart(parent->theRestartFile(), "Tracer");
    }
#endif
}

//
//Do work after init().
//
void EM::post_init(Real stop_time) {
    if (level > 0)
        return;
    //
    // Average data down from finer levels
    // so that conserved data is consistent between levels.
    //
    int finest_level = parent->finestLevel();
    for (int k = finest_level - 1; k >= 0; k--)
        getLevel(k).avgDown();
}

//
//Error estimation for regridding.
//

void EM::errorEst(TagBoxArray &tags,
                  int clearval,
                  int tagval,
                  Real time,
                  int n_error_buf,
                  int ngrow) {
    const Real *dx = geom.CellSize();
    const Real *prob_lo = geom.ProbLo();

    MultiFab &S_new = get_new_data(State_Type);

    ngrow = 1;
//    MultiFab& Energy_dens = *(derive("Energy_dens",time,ngrow));//
    auto Energy_dens = derive("Energy_dens", time, ngrow);//
    // Energy density with a ghost cell
    MultiFab Eborder(grids, dmap, 1, 1);
    MultiFab::Copy(Eborder, *Energy_dens, 0, 0, 1, 1);

    // epx,epy,epz,sqrtepx,sqrtepy,sqrtepz 
    // mux,muy,muz,sqrtmux,sqrtmuy,sqrtmuz 
    FArrayBox ep, mu;

#ifdef _OPENMP
#pragma omp parallel
#endif
    {
        Vector<int> itags;

//	for (MFIter mfi(S_new,true); mfi.isValid(); ++mfi)
        for (MFIter mfi(*Energy_dens, true); mfi.isValid(); ++mfi) {
            const Box &tilebx = mfi.tilebox();

            TagBox &tagfab = tags[mfi];

            FArrayBox &E_grown = Eborder[mfi];

            // We cannot pass tagfab to Fortran becuase it is BaseFab<char>.
            // So we are going to get a temporary integer array.
            tagfab.get_itags(itags, tilebx);

            // data pointer and index space
            int *tptr = itags.dataPtr();
            const int *tlo = tilebx.loVect();
            const int *thi = tilebx.hiVect();

            state_error(tptr, ARLIM_3D(tlo), ARLIM_3D(thi),
//		   BL_TO_FORTRAN_N_3D(S_new[mfi],1),
//			BL_TO_FORTRAN_3D(S_new[mfi]),
//			BL_TO_FORTRAN_3D(Energy_dens[mfi]),
                        BL_TO_FORTRAN_3D(E_grown),
                        &tagval, &clearval,
                        ARLIM_3D(tilebx.loVect()), ARLIM_3D(tilebx.hiVect()),
                        ZFILL(dx), ZFILL(prob_lo), &time, &level);

#ifdef TAGGING_EP_MU
            //
            // Tag materials
            //
            ep.resize(amrex::grow(tilebx, NUM_GROW), 6);
            mu.resize(amrex::grow(tilebx, NUM_GROW), 6);

            get_ep_mu(prob_lo, tilebx.loVect(), tilebx.hiVect(),
                      BL_TO_FORTRAN_3D(S_new[mfi]),
                      BL_TO_FORTRAN_3D(ep),
                      BL_TO_FORTRAN_3D(mu),
                      dx, NUM_GROW, NUM_STATE, is_D_wave, time);

            materials_error(tptr, ARLIM_3D(tlo), ARLIM_3D(thi),
                            BL_TO_FORTRAN_3D(ep),
                            BL_TO_FORTRAN_3D(mu),
                            &tagval, &clearval,
                            ARLIM_3D(tilebx.loVect()), ARLIM_3D(tilebx.hiVect()),
                            ZFILL(dx), ZFILL(prob_lo), &time, &level);
#endif

#ifdef TAGGING_SOURCES
            //
            // Tag sources
            //
            for (int s = 0; s < f_sources.size(); s++) {
                const RealBox sl = f_sources.get_source_limits(s);
                sources_error(tptr, ARLIM_3D(tlo), ARLIM_3D(thi),
                              sl.lo(), sl.hi(),
                              &tagval, &clearval,
                              ARLIM_3D(tilebx.loVect()), ARLIM_3D(tilebx.hiVect()),
                              ZFILL(dx), ZFILL(prob_lo), &time, &level);
            }
#endif

#ifdef TAGGING_POLARIZATIONS
#if N_POLARIZATIONS > 0
            //
            // Tag polarizations
            //
            for (int s = 0; s < Polarizations.size(); s++) {
                const RealBox pl = Polarizations.get_pols_limits(s);
                pol_error(tptr, ARLIM_3D(tlo), ARLIM_3D(thi),
                          pl.lo(), pl.hi(),
                          &tagval, &clearval,
                          ARLIM_3D(tilebx.loVect()), ARLIM_3D(tilebx.hiVect()),
                          ZFILL(dx), ZFILL(prob_lo), &time, &level);
            }
#endif
#endif

#ifdef FORCE_TAGGING
            force_tagging_error(tptr, ARLIM_3D(tlo), ARLIM_3D(thi),
                                &tagval, &clearval,
                                ARLIM_3D(tilebx.loVect()), ARLIM_3D(tilebx.hiVect()),
                                ZFILL(dx), ZFILL(prob_lo), &time, &level);
#endif
            //
            // Now update the tags in the TagBox.
            //
            tagfab.tags_and_untags(itags, tilebx);
        }
    }
}


std::unique_ptr<MultiFab> EM::derive(const std::string &name,
                                     Real time,
                                     int ngrow) {
#ifdef PARTICLES
    return ParticleDerive(name,time,ngrow);
#else
    return AmrLevel::derive(name, time, ngrow);
#endif
}

void EM::derive(const std::string &name,
                Real time,
                MultiFab &mf,
                int dcomp) {
    AmrLevel::derive(name, time, mf, dcomp);
}


void EM::read_params() {
    static bool done = false;

    if (done) return;

    done = true;

    ParmParse pp("em");

    pp.query("v", verbose);
    pp.query("cfl", cfl);
    pp.query("do_reflux", do_reflux);
    pp.query("is_D_wave", is_D_wave);

    Geometry const *gg = AMReX::top()->getDefaultGeometry();

    // This tutorial code only supports Cartesian coordinates.
    if (!gg->IsCartesian()) {
        amrex::Abort("Please set geom.coord_sys = 0");
    }

    // This code only supports Cartesian coordinates.
//    if (! Geometry::IsCartesian()) {
//	amrex::Abort("Please set geom.coord_sys = 0");
//    }

#ifdef PARTICLES
    pp.query("do_tracers", do_tracers);
#endif


    //
    // read tagging parameters from probin file
    //

    std::string probin_file("probin");

    ParmParse ppa("amr");
    ppa.query("probin_file", probin_file);

    int probin_file_length = probin_file.length();
    Vector<int> probin_file_name(probin_file_length);

    for (int i = 0; i < probin_file_length; i++)
        probin_file_name[i] = probin_file[i];

    // use a fortran routine to
    // read in tagging parameters from probin file
    get_tagging_params(probin_file_name.dataPtr(), &probin_file_length);

}

void EM::reflux() {
    BL_ASSERT(level < parent->finestLevel());

    const Real strt = ParallelDescriptor::second();

    getFluxReg(level + 1).Reflux(get_new_data(State_Type), 1.0, 0, 0, NUM_STATE, geom);

    if (verbose) {
        const int IOProc = ParallelDescriptor::IOProcessorNumber();
        Real end = ParallelDescriptor::second() - strt;

        ParallelDescriptor::ReduceRealMax(end, IOProc);

        amrex::Print() << "EM::reflux() at level " << level
                       << " : time = " << end << std::endl;
    }
}

void EM::avgDown() {
    if (level == parent->finestLevel()) return;
    avgDown(State_Type);
}

void EM::avgDown(int state_indx) {
    if (level == parent->finestLevel()) return;

    EM &fine_lev = getLevel(level + 1);
    MultiFab &S_fine = fine_lev.get_new_data(state_indx);
    MultiFab &S_crse = get_new_data(state_indx);

    amrex::average_down(S_fine, S_crse,
                        fine_lev.geom, geom,
                        0, S_fine.nComp(), parent->refRatio(level));
}

#ifdef PARTICLES
void
EM::init_particles ()
{
  if (do_tracers and level == 0)
    {
      BL_ASSERT(TracerPC == nullptr);
      
      TracerPC.reset(new AmrTracerParticleContainer(parent));
      TracerPC->do_tiling = true;
      TracerPC->tile_size = IntVect(AMREX_D_DECL(1024000,4,4));

      const BoxArray& ba = TracerPC->ParticleBoxArray(0);
      const DistributionMapping& dm = TracerPC->ParticleDistributionMap(0);

      AmrTracerParticleContainer::ParticleInitData pdata = {1.0};

      TracerPC->SetVerbose(0);
      TracerPC->InitOnePerCell(0.5, 0.5, 0.5, pdata);

      TracerPC->Redistribute();
    }
}
#endif
