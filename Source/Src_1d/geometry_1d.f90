! Created by ovidio on 11/10/20.

function SEGMENT(x, pxlo, pxhi) result(is_inside)
    double precision, intent(in) :: x, pxlo, pxhi
    logical :: is_inside

    is_inside = ((x>=pxlo).and.(x<=pxhi))
end function SEGMENT
