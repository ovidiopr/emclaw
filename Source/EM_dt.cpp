// license_AMReX.txt
#include <EM.H>
#include <EM_F.H>

using namespace amrex;

Real EM::initialTimeStep() {
    return estTimeStep(0.0);
}

Real EM::estTimeStep(Real) {
    // This is just a dummy value to start with 
    Real dt_est = 1.0e+20;

    const Real *dx = geom.CellSize();
    const Real *prob_lo = geom.ProbLo();
    const Real cur_time = state[State_Type].curTime();
    MultiFab &S_new = get_new_data(State_Type);

#ifdef _OPENMP
#pragma omp parallel reduction(min:dt_est)
#endif
    {
        // epx,epy,epz,sqrtepx,sqrtepy,sqrtepz
        // mux,muy,muz,sqrtmux,sqrtmuy,sqrtmuz
        FArrayBox ep, mu;

        for (MFIter mfi(S_new, true); mfi.isValid(); ++mfi) {
            const Box &box = mfi.validbox();
            const int *lo = box.loVect();
            const int *hi = box.hiVect();
            ep.resize(amrex::grow(box, NUM_GROW), 6);
            mu.resize(amrex::grow(box, NUM_GROW), 6);

            get_ep_mu(prob_lo, lo, hi,
                      BL_TO_FORTRAN_3D(S_new[mfi]),
                      BL_TO_FORTRAN_3D(ep),
                      BL_TO_FORTRAN_3D(mu),
                      dx, NUM_GROW, NUM_STATE, is_D_wave, 0.0);

// We do get_ep_mu and check
            for (int i = 0; i < BL_SPACEDIM; ++i) {
//		Real umax = 1.0;
                Real umax = 1.0 / (ep.min(i + 3) * mu.min(i + 3));
                if (umax > 1.e-100) {
                    dt_est = std::min(dt_est, dx[i] / umax);
                }
            }
        }
    }

    ParallelDescriptor::ReduceRealMin(dt_est);
    dt_est *= cfl;

    if (verbose && ParallelDescriptor::IOProcessor())
        std::cout << "EM::estTimeStep at level " << level << ":  dt_est = " << dt_est << std::endl;

    return dt_est;
}

void EM::computeNewDt(int finest_level,
                      int sub_cycle,
                      Vector<int> &n_cycle,
                      const Vector<IntVect> &ref_ratio,
                      Vector<Real> &dt_min,
                      Vector<Real> &dt_level,
                      Real stop_time,
                      int post_regrid_flag) {
    //
    // We are at the end of a coarse grid timecycle.
    // Compute the timesteps for the next iteration.
    //
    if (level > 0)
        return;

    for (int i = 0; i <= finest_level; i++) {
        EM &em_level = getLevel(i);
        dt_min[i] = em_level.estTimeStep(dt_level[i]);
    }

    if (post_regrid_flag == 1) {
        //
        // Limit dt's by pre-regrid dt
        //
        for (int i = 0; i <= finest_level; i++) {
            dt_min[i] = std::min(dt_min[i], dt_level[i]);
        }
    } else {
        //
        // Limit dt's by change_max * old dt
        //
        static Real change_max = 1.1;
        for (int i = 0; i <= finest_level; i++) {
            dt_min[i] = std::min(dt_min[i], change_max * dt_level[i]);
        }
    }

    //
    // Find the minimum over all levels
    //
    Real dt_0 = 1.0e+100;
    int n_factor = 1;
    for (int i = 0; i <= finest_level; i++) {
        n_factor *= n_cycle[i];
        dt_0 = std::min(dt_0, n_factor * dt_min[i]);
    }

    //
    // Limit dt's by the value of stop_time.
    //
    const Real eps = 0.001 * dt_0;
    Real cur_time = state[State_Type].curTime();
    if (stop_time >= 0.0) {
        if ((cur_time + dt_0) > (stop_time - eps))
            dt_0 = stop_time - cur_time;
    }

    n_factor = 1;
    for (int i = 0; i <= finest_level; i++) {
        n_factor *= n_cycle[i];
        dt_level[i] = dt_0 / n_factor;
    }
}

void EM::computeInitialDt(int finest_level,
                          int sub_cycle,
                          Vector<int> &n_cycle,
                          const Vector<IntVect> &ref_ratio,
                          Vector<Real> &dt_level,
                          Real stop_time) {
    //
    // Grids have been constructed, compute dt for all levels.
    //
    if (level > 0)
        return;

    Real dt_0 = 1.0e+100;
    int n_factor = 1;
    for (int i = 0; i <= finest_level; i++) {
        dt_level[i] = getLevel(i).initialTimeStep();
        n_factor *= n_cycle[i];
        dt_0 = std::min(dt_0, n_factor * dt_level[i]);
    }

    //
    // Limit dt's by the value of stop_time.
    //
    const Real eps = 0.001 * dt_0;
    Real cur_time = state[State_Type].curTime();
    if (stop_time >= 0.0) {
        if ((cur_time + dt_0) > (stop_time - eps))
            dt_0 = stop_time - cur_time;
    }

    n_factor = 1;
    for (int i = 0; i <= finest_level; i++) {
        n_factor *= n_cycle[i];
        dt_level[i] = dt_0 / n_factor;
    }
}
