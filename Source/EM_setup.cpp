// license_EMcLAW.txt
//#include <fstream>
//#include <iomanip>

#include <EM.H>
#include <EM_F.H>
#include <AMReX_ParmParse.H>
#include <AMReX_BC_TYPES.H>
#include <Derive_F.H>

#include <sstream>
#include "DEFINES.H"

using namespace amrex;

//#include <AMReX_Geometry.H>
//#include <AMReX_VisMF.H>

static Box the_same_box(const Box &b) { return b; }

static Box grow_box_by_one(const Box &b) { return amrex::grow(b, 1); }

void
EM::variableCleanUp() {
    desc_lst.clear();
#ifdef PARTICLES
    TracerPC.reset();
#endif
}

void
EM::variableSetUp() {
    // Get options, set phys_bc
    read_params();

#if (BL_SPACEDIM == 1)

    NUM_STATE = 4 + N_POLARIZATIONS + N_POL_LORENTZ + 40*N_POL_HN;
    NUM_STATE_U = 2;

#elif (BL_SPACEDIM == 2)

    if (is_D_wave == 1) {
    NUM_STATE = 7 + 2*(N_POLARIZATIONS + N_POL_LORENTZ + 40*N_POL_HN);
    } else{
    NUM_STATE = 7 + N_POLARIZATIONS + N_POL_LORENTZ + 40*N_POL_HN + 2*N_POL_ION;
    }
#ifdef DIV_CONTROL
    NUM_STATE_U = 4;
#else
    NUM_STATE_U = 3;
#endif

#else
    NUM_STATE = 14 + 3 * (N_POLARIZATIONS + N_POL_LORENTZ + 40 * N_POL_HN) + 4*N_POL_ION;
#ifdef DIV_CONTROL
    NUM_STATE_U = 8;
#else
    NUM_STATE_U = 6;
#endif

#endif

    BL_ASSERT(desc_lst.size() == 0);


    desc_lst.addDescriptor(State_Type, IndexType::TheCellType(),
                           StateDescriptor::Point, 0, NUM_STATE,
                           &cell_cons_interp);//&quartic_interp);

    Vector<int> lo_bc(BL_SPACEDIM);
    Vector<int> hi_bc(BL_SPACEDIM);
    for (int i = 0; i < BL_SPACEDIM; ++i) {
        lo_bc[i] = hi_bc[i] = INT_DIR;   // periodic boundaries
    }

    ParmParse ppbc;
    ppbc.queryarr("lo_bc", lo_bc, 0, BL_SPACEDIM);///
    ppbc.queryarr("hi_bc", hi_bc, 0, BL_SPACEDIM);///

    BCRec bc(&lo_bc[0], &hi_bc[0]);

    int metallic_walls;
    ppbc.query("metallic_walls", metallic_walls);
    Vector<int> lo_bcDx(BL_SPACEDIM);
    Vector<int> hi_bcDx(BL_SPACEDIM);
    Vector<int> lo_bcDy(BL_SPACEDIM);
    Vector<int> hi_bcDy(BL_SPACEDIM);
    Vector<int> lo_bcDz(BL_SPACEDIM);
    Vector<int> hi_bcDz(BL_SPACEDIM);
    Vector<int> lo_bcBx(BL_SPACEDIM);
    Vector<int> hi_bcBx(BL_SPACEDIM);
    Vector<int> lo_bcBy(BL_SPACEDIM);
    Vector<int> hi_bcBy(BL_SPACEDIM);
    Vector<int> lo_bcBz(BL_SPACEDIM);
    Vector<int> hi_bcBz(BL_SPACEDIM);
// For metallic boundaries
    if (metallic_walls == 1) {
        ppbc.queryarr("lo_bcDx", lo_bcDx, 0, BL_SPACEDIM);
        ppbc.queryarr("hi_bcDx", hi_bcDx, 0, BL_SPACEDIM);
        ppbc.queryarr("lo_bcDy", lo_bcDy, 0, BL_SPACEDIM);
        ppbc.queryarr("hi_bcDy", hi_bcDy, 0, BL_SPACEDIM);
        ppbc.queryarr("lo_bcDz", lo_bcDz, 0, BL_SPACEDIM);
        ppbc.queryarr("hi_bcDz", hi_bcDz, 0, BL_SPACEDIM);
        ppbc.queryarr("lo_bcBx", lo_bcBx, 0, BL_SPACEDIM);
        ppbc.queryarr("hi_bcBx", hi_bcBx, 0, BL_SPACEDIM);
        ppbc.queryarr("lo_bcBy", lo_bcBy, 0, BL_SPACEDIM);
        ppbc.queryarr("hi_bcBy", hi_bcBy, 0, BL_SPACEDIM);
        ppbc.queryarr("lo_bcBz", lo_bcBz, 0, BL_SPACEDIM);
        ppbc.queryarr("hi_bcBz", hi_bcBz, 0, BL_SPACEDIM);
    } else {
        lo_bcDx = lo_bc;
        hi_bcDx = hi_bc;
        lo_bcDy = lo_bc;
        hi_bcDy = hi_bc;
        lo_bcDz = lo_bc;
        hi_bcDz = hi_bc;
        lo_bcBx = lo_bc;
        hi_bcBx = hi_bc;
        lo_bcBy = lo_bc;
        hi_bcBy = hi_bc;
        lo_bcBz = lo_bc;
        hi_bcBz = hi_bc;
    }

    BCRec bcDx(&lo_bcDx[0], &hi_bcDx[0]);
    BCRec bcDy(&lo_bcDy[0], &hi_bcDy[0]);
    BCRec bcDz(&lo_bcDz[0], &hi_bcDz[0]);
    BCRec bcBx(&lo_bcBx[0], &hi_bcBx[0]);
    BCRec bcBy(&lo_bcBy[0], &hi_bcBy[0]);
    BCRec bcBz(&lo_bcBz[0], &hi_bcBz[0]);

// Polarization variables
    std::stringstream NamePol;

#if (BL_SPACEDIM == 1)


    desc_lst.setComponent(State_Type, 0, "Dy", bcDy, 
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 1, "Bz", bcBz,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 2, "epy", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 3, "muz", bc,
              StateDescriptor::BndryFunc(emfill));

// Polarization variables
    for (int i=1; i<=N_POLARIZATIONS ; i++){
     NamePol << "P" << i << "y";
     desc_lst.setComponent(State_Type, 3+i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }

// Extra polarization variables
    for (int i=1; i<=N_POL_LORENTZ ; i++){

     NamePol << "dP/dt" << i << "y";
     desc_lst.setComponent(State_Type, 3+N_POLARIZATIONS+i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }
    for (int i=1; i<=40*N_POL_HN ; i++){

     NamePol << "Phi_HN" << i << "y";
     desc_lst.setComponent(State_Type, 3+N_POLARIZATIONS+N_POL_LORENTZ+i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }


#elif (BL_SPACEDIM == 2)


    if (is_D_wave) { //D-wave

    desc_lst.setComponent(State_Type, 0, "Dx", bcDx, 
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 1, "Dy", bcDy,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 2, "Bz", bcBz,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 3, "Phi", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 4, "epx", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 5, "epy", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 6, "muz", bc,
              StateDescriptor::BndryFunc(emfill));

// Polarization variables
    for (int i=1; i<=N_POLARIZATIONS ; i++){
     NamePol << "P" << i << "x";
     desc_lst.setComponent(State_Type, 6+2*i-1, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());

     NamePol << "P" << i << "y";
     desc_lst.setComponent(State_Type, 6+2*i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }

// Extra polarization variables
    for (int i=1; i<=N_POL_LORENTZ ; i++){
     NamePol << "dP/dt" << i << "x";
     desc_lst.setComponent(State_Type, 6+2*N_POLARIZATIONS+2*i-1, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());

     NamePol << "dP/dt" << i << "y";
     desc_lst.setComponent(State_Type, 6+2*N_POLARIZATIONS+2*i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }
    for (int i=1; i<=40*N_POL_HN ; i++){
     NamePol << "Phi_HN" << i << "x";
     desc_lst.setComponent(State_Type, 6+2*N_POLARIZATIONS+2*N_POL_LORENTZ+2*i-1, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());

     NamePol << "Phi_HN" << i << "y";
     desc_lst.setComponent(State_Type, 6+2*N_POLARIZATIONS+2*N_POL_LORENTZ+2*i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }


    } else{ //B-wave


    desc_lst.setComponent(State_Type, 0, "Bx", bcBx,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 1, "By", bcBy,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 2, "Dz", bcDz,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 3, "Psi", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 4, "mux", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 5, "muy", bc,
              StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 6, "epz", bc,
              StateDescriptor::BndryFunc(emfill));

// Polarization variables
    for (int i=1; i<=N_POLARIZATIONS ; i++){
     NamePol << "P" << i << "z";
     desc_lst.setComponent(State_Type, 6+i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }

// Extra polarization variables
    for (int i=1; i<=N_POL_LORENTZ ; i++){
     NamePol << "dP/dt" << i << "z";
     desc_lst.setComponent(State_Type, 6+N_POLARIZATIONS+i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }
    for (int i=1; i<=40*N_POL_HN ; i++){
     NamePol << "Phi_HN" << i << "z";
     desc_lst.setComponent(State_Type, 6+N_POLARIZATIONS+N_POL_LORENTZ+i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }
    for (int i=1; i<=N_POL_ION ; i++){
     NamePol << "dP/dt" << i << "z";
     desc_lst.setComponent(State_Type, 6+N_POLARIZATIONS+N_POL_LORENTZ+40*N_POL_HN+2*i-1, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
     NamePol << "Ne" << i;
     desc_lst.setComponent(State_Type, 6+N_POLARIZATIONS+N_POL_LORENTZ+40*N_POL_HN+2*i, NamePol.str(), bc,
              StateDescriptor::BndryFunc(emfill));
     NamePol.str(std::string());
    }

    } //D-wave or B-wave


#else


    desc_lst.setComponent(State_Type, 0, "Dx", bcDx,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 1, "Dy", bcDy,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 2, "Dz", bcDz,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 3, "Bx", bcBx,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 4, "By", bcBy,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 5, "Bz", bcBz,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 6, "Phi", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 7, "Psi", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 8, "epx", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 9, "epy", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 10, "epz", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 11, "mux", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 12, "muy", bc,
                          StateDescriptor::BndryFunc(emfill));

    desc_lst.setComponent(State_Type, 13, "muz", bc,
                          StateDescriptor::BndryFunc(emfill));

// Polarization variables
    for (int i = 1; i <= N_POLARIZATIONS; i++) {
        NamePol << "P" << i << "x";
        desc_lst.setComponent(State_Type, 13 + 3 * i - 2, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

        NamePol << "P" << i << "y";
        desc_lst.setComponent(State_Type, 13 + 3 * i - 1, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

        NamePol << "P" << i << "z";
        desc_lst.setComponent(State_Type, 13 + 3 * i, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());
    }

// Extra polarization variables
    for (int i = 1; i <= N_POL_LORENTZ; i++) {
        NamePol << "dP/dt" << i << "x";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3 * i - 2, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

        NamePol << "dP/dt" << i << "y";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3 * i - 1, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

        NamePol << "dP/dt" << i << "z";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3 * i, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());
    }
    for (int i = 1; i <= 40 * N_POL_HN; i++) {
        NamePol << "Phi_HN" << i << "x";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3 * N_POL_LORENTZ + 3 * i - 2, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

        NamePol << "Phi_HN" << i << "y";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3 * N_POL_LORENTZ + 3 * i - 1, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

        NamePol << "Phi_HN" << i << "z";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3 * N_POL_LORENTZ + 3 * i, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());
    }
    for (int i = 1; i <= N_POL_ION; i++) {
        NamePol << "dP/dt" << i << "x";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3*N_POL_LORENTZ + 3*40*N_POL_HN + 4 * i - 3, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

	NamePol << "dP/dt" << i << "y";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3*N_POL_LORENTZ + 3*40*N_POL_HN + 4 * i - 2, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

	NamePol << "dP/dt" << i << "z";
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3*N_POL_LORENTZ + 3*40*N_POL_HN + 4 * i - 1, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

	NamePol << "Ne" << i;
        desc_lst.setComponent(State_Type, 13 + 3 * N_POLARIZATIONS + 3*N_POL_LORENTZ + 3*40*N_POL_HN + 4 * i, NamePol.str(), bc,
                              StateDescriptor::BndryFunc(emfill));
        NamePol.str(std::string());

    }
#endif


    //
    // derived quantities
    //

    derive_lst.add("Energy_dens", IndexType::TheCellType(),
                   1, ca_energy_dens, the_same_box, &cell_cons_interp);

    derive_lst.addComponent("Energy_dens",
                            desc_lst, State_Type, 0, NUM_STATE);

#if (BL_SPACEDIM == 1)


    derive_lst.add("TPy",IndexType::TheCellType(),
    1,ca_TPy,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("TPy",
    desc_lst,State_Type,0,NUM_STATE);

#ifdef SHOW_E
    derive_lst.add("Ey",IndexType::TheCellType(),
    1,ca_Ey,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Ey",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_D
    derive_lst.add("DPy",IndexType::TheCellType(),
    1,ca_Dy,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("DPy",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_H
    derive_lst.add("Hz",IndexType::TheCellType(),
    1,ca_Hz,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Hz",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_POYNTING
    derive_lst.add("Sx",IndexType::TheCellType(),
    1,ca_Sx_D_wave,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Sx",
    desc_lst,State_Type,0,NUM_STATE);
#endif


#elif (BL_SPACEDIM == 2)


    if (is_D_wave) {

    derive_lst.add("TPx",IndexType::TheCellType(),
    1,ca_TPx,the_same_box,&cell_cons_interp);

    derive_lst.add("TPy",IndexType::TheCellType(),
    1,ca_TPy,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("TPx",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("TPy",
    desc_lst,State_Type,0,NUM_STATE);

#ifdef SHOW_E
    derive_lst.add("Ex",IndexType::TheCellType(),
    1,ca_Ex,the_same_box,&cell_cons_interp);

    derive_lst.add("Ey",IndexType::TheCellType(),
    1,ca_Ey,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Ex",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("Ey",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_D
    derive_lst.add("DPx",IndexType::TheCellType(),
    1,ca_Dx,the_same_box,&cell_cons_interp);

    derive_lst.add("DPy",IndexType::TheCellType(),
    1,ca_Dy,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("DPx",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("DPy",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_H
    derive_lst.add("Hz",IndexType::TheCellType(),
    1,ca_Hz,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Hz",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_DIV_D
    derive_lst.add("Div_D",IndexType::TheCellType(),
    1,ca_div_D,grow_box_by_one,&cell_cons_interp);

    derive_lst.addComponent("Div_D",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_POYNTING
    derive_lst.add("Sx",IndexType::TheCellType(),
    1,ca_Sx_D_wave,the_same_box,&cell_cons_interp);

    derive_lst.add("Sy",IndexType::TheCellType(),
    1,ca_Sy_D_wave,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Sx",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("Sy",
    desc_lst,State_Type,0,NUM_STATE);
#endif

    } else{

    derive_lst.add("TPz",IndexType::TheCellType(),
    1,ca_TPz,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("TPz",
    desc_lst,State_Type,0,NUM_STATE);

#ifdef SHOW_E
    derive_lst.add("Ez",IndexType::TheCellType(),
    1,ca_Ez,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Ez",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_D
    derive_lst.add("DPz",IndexType::TheCellType(),
    1,ca_Dz,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("DPz",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_H
    derive_lst.add("Hx",IndexType::TheCellType(),
    1,ca_Hx,the_same_box,&cell_cons_interp);

    derive_lst.add("Hy",IndexType::TheCellType(),
    1,ca_Hy,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Hx",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("Hy",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_DIV_B
    derive_lst.add("Div_B",IndexType::TheCellType(),
    1,ca_div_B,grow_box_by_one,&cell_cons_interp);

    derive_lst.addComponent("Div_B",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_POYNTING
    derive_lst.add("Sx",IndexType::TheCellType(),
    1,ca_Sx_B_wave,the_same_box,&cell_cons_interp);

    derive_lst.add("Sy",IndexType::TheCellType(),
    1,ca_Sy_B_wave,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("Sx",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("Sy",
    desc_lst,State_Type,0,NUM_STATE);
#endif

    }


#else


    derive_lst.add("TPx", IndexType::TheCellType(),
                   1, ca_TPx, the_same_box, &cell_cons_interp);

    derive_lst.add("TPy", IndexType::TheCellType(),
                   1, ca_TPy, the_same_box, &cell_cons_interp);

    derive_lst.add("TPz", IndexType::TheCellType(),
                   1, ca_TPz, the_same_box, &cell_cons_interp);

    derive_lst.addComponent("TPx",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("TPy",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("TPz",
                            desc_lst, State_Type, 0, NUM_STATE);

#ifdef SHOW_E
    derive_lst.add("Ex", IndexType::TheCellType(),
                   1, ca_Ex, the_same_box, &cell_cons_interp);

    derive_lst.add("Ey", IndexType::TheCellType(),
                   1, ca_Ey, the_same_box, &cell_cons_interp);

    derive_lst.add("Ez", IndexType::TheCellType(),
                   1, ca_Ez, the_same_box, &cell_cons_interp);

    derive_lst.addComponent("Ex",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("Ey",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("Ez",
                            desc_lst, State_Type, 0, NUM_STATE);
#endif

#ifdef SHOW_D
    derive_lst.add("DPx",IndexType::TheCellType(),
    1,ca_Dx,the_same_box,&cell_cons_interp);

    derive_lst.add("DPy",IndexType::TheCellType(),
    1,ca_Dy,the_same_box,&cell_cons_interp);

    derive_lst.add("DPz",IndexType::TheCellType(),
    1,ca_Dz,the_same_box,&cell_cons_interp);

    derive_lst.addComponent("DPx",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("DPy",
    desc_lst,State_Type,0,NUM_STATE);

    derive_lst.addComponent("DPz",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_H
    derive_lst.add("Hx", IndexType::TheCellType(),
                   1, ca_Hx, the_same_box, &cell_cons_interp);

    derive_lst.add("Hy", IndexType::TheCellType(),
                   1, ca_Hy, the_same_box, &cell_cons_interp);

    derive_lst.add("Hz", IndexType::TheCellType(),
                   1, ca_Hz, the_same_box, &cell_cons_interp);

    derive_lst.addComponent("Hx",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("Hy",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("Hz",
                            desc_lst, State_Type, 0, NUM_STATE);
#endif

#ifdef SHOW_DIV_B
    derive_lst.add("Div_B",IndexType::TheCellType(),
    1,ca_div_B,grow_box_by_one,&cell_cons_interp);

    derive_lst.addComponent("Div_B",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_DIV_D
    derive_lst.add("Div_D",IndexType::TheCellType(),
    1,ca_div_D,grow_box_by_one,&cell_cons_interp);

    derive_lst.addComponent("Div_D",
    desc_lst,State_Type,0,NUM_STATE);
#endif

#ifdef SHOW_POYNTING
    derive_lst.add("Sx", IndexType::TheCellType(),
                   1, ca_Sx_D_wave, the_same_box, &cell_cons_interp);

    derive_lst.add("Sy", IndexType::TheCellType(),
                   1, ca_Sy_D_wave, the_same_box, &cell_cons_interp);

    derive_lst.add("Sz", IndexType::TheCellType(),
                   1, ca_Sz, the_same_box, &cell_cons_interp);

    derive_lst.addComponent("Sx",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("Sy",
                            desc_lst, State_Type, 0, NUM_STATE);

    derive_lst.addComponent("Sz",
                            desc_lst, State_Type, 0, NUM_STATE);
#endif


#endif
}
