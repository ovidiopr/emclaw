// license_AMReX.txt
// license_EMcLAW.txt
#include <EM.H>
#include <AMReX_Utility.H>

#include "DEFINES.H"

using namespace amrex;

std::string EM::thePlotFileType() const {
    static const std::string the_plot_file_type("HyperCLaw-V1.1");
    return the_plot_file_type;
}

void EM::setPlotVariables() {
    AmrLevel::setPlotVariables();

// Energy_dens will always be plotted
    parent->addDerivePlotVar("Energy_dens");

#ifdef SHOW_E

#if (BL_SPACEDIM == 1)

    parent->addDerivePlotVar("Ey");

#elif (BL_SPACEDIM == 2)

    if (is_D_wave) {

     parent->addDerivePlotVar("Ex");
     parent->addDerivePlotVar("Ey");

    } else{

     parent->addDerivePlotVar("Ez");

    }

#else

    parent->addDerivePlotVar("Ex");
    parent->addDerivePlotVar("Ey");
    parent->addDerivePlotVar("Ez");

#endif

#endif

#ifdef SHOW_D

#if (BL_SPACEDIM == 1)

    parent->addDerivePlotVar("DPy");

#elif (BL_SPACEDIM == 2)

   if (is_D_wave) {

    parent->addDerivePlotVar("DPx");
    parent->addDerivePlotVar("DPy");

   } else{

    parent->addDerivePlotVar("DPz");

   }

#else

   parent->addDerivePlotVar("DPx");
   parent->addDerivePlotVar("DPy");
   parent->addDerivePlotVar("DPz");

#endif

#endif

#ifdef SHOW_H

#if (BL_SPACEDIM == 1)

    parent->addDerivePlotVar("Hz");

#elif (BL_SPACEDIM == 2)

    if (is_D_wave) {

     parent->addDerivePlotVar("Hz");

    } else{

     parent->addDerivePlotVar("Hx");
     parent->addDerivePlotVar("Hy");

    }

#else

    parent->addDerivePlotVar("Hx");
    parent->addDerivePlotVar("Hy");
    parent->addDerivePlotVar("Hz");

#endif

#endif

#ifdef SHOW_DIV_B

#if (BL_SPACEDIM == 1)

#elif (BL_SPACEDIM == 2)

    if (is_D_wave) {

    } else{

     parent->addDerivePlotVar("Div_B");

    }

#else

    parent->addDerivePlotVar("Div_B");

#endif

#endif

#ifdef SHOW_DIV_D

#if (BL_SPACEDIM == 1)

#elif (BL_SPACEDIM == 2)

    if (is_D_wave) {

     parent->addDerivePlotVar("Div_D");

    } else{

    }

#else

    parent->addDerivePlotVar("Div_D");

#endif

#endif

#ifdef SHOW_POYNTING

#if (BL_SPACEDIM == 1)

    parent->addDerivePlotVar("Sx");

#elif (BL_SPACEDIM == 2)

    parent->addDerivePlotVar("Sx");
    parent->addDerivePlotVar("Sy");

#else

    parent->addDerivePlotVar("Sx");
    parent->addDerivePlotVar("Sy");
    parent->addDerivePlotVar("Sz");

#endif

#endif

#if N_POLARIZATIONS > 0

#if (BL_SPACEDIM == 1)

    parent->addDerivePlotVar("TPy");

#elif (BL_SPACEDIM == 2)

    if (is_D_wave) {

     parent->addDerivePlotVar("TPx");
     parent->addDerivePlotVar("TPy");

    } else{

     parent->addDerivePlotVar("TPz");

    }

#else

    parent->addDerivePlotVar("TPx");
    parent->addDerivePlotVar("TPy");
    parent->addDerivePlotVar("TPz");

#endif

#endif

}

void EM::writePlotFile(const std::string &dir,
                       std::ostream &os,
                       VisMF::How how) {
    int i, n;
    //
    // The list of indices of State to write to plotfile.
    // first component of pair is state_type,
    // second component of pair is component # within the state_type
    //
    std::vector<std::pair<int, int> > plot_var_map;
    for (int typ = 0; typ < desc_lst.size(); typ++)
        for (int comp = 0; comp < desc_lst[typ].nComp(); comp++)
            if (parent->isStatePlotVar(desc_lst[typ].name(comp)) &&
                desc_lst[typ].getType() == IndexType::TheCellType())
                plot_var_map.push_back(std::pair<int, int>(typ, comp));


    int num_derive = 0;

    std::list<std::string> derive_names;
    const std::list<DeriveRec> &dlist = derive_lst.dlist();

    for (std::list<DeriveRec>::const_iterator it = dlist.begin();
         it != dlist.end(); ++it) {
        if (parent->isDerivePlotVar(it->name())) {
            derive_names.push_back(it->name());
            num_derive++;
        }
    }

    int n_data_items = plot_var_map.size() + num_derive;
//    int n_data_items = plot_var_map.size();

    Real cur_time = state[State_Type].curTime();

    if (level == 0 && ParallelDescriptor::IOProcessor()) {
        //
        // The first thing we write out is the plotfile type.
        //
        os << thePlotFileType() << '\n';

        if (n_data_items == 0)
            amrex::Error("Must specify at least one valid data item to plot");

        os << n_data_items << '\n';

        Geometry const *gg = AMReX::top()->getDefaultGeometry();

        // This tutorial code only supports Cartesian coordinates.
//    if (! gg->IsCartesian()) {
//        amrex::Abort("Please set geom.coord_sys = 0");
//    }


        //
        // Names of variables. Fisrt state and then derived
        //
        for (i = 0; i < plot_var_map.size(); i++) {
            int typ = plot_var_map[i].first;
            int comp = plot_var_map[i].second;
            os << desc_lst[typ].name(comp) << '\n';
        }

        for (std::list<std::string>::iterator it = derive_names.begin();
             it != derive_names.end(); ++it) {
            const DeriveRec *rec = derive_lst.get(*it);
            os << rec->variableName(0) << '\n';
        }


        os << BL_SPACEDIM << '\n';
        os << parent->cumTime() << '\n';
        int f_lev = parent->finestLevel();
        os << f_lev << '\n';
        for (i = 0; i < BL_SPACEDIM; i++)
            os << gg->ProbLo(i) << ' ';
        os << '\n';
        for (i = 0; i < BL_SPACEDIM; i++)
            os << gg->ProbHi(i) << ' ';
        os << '\n';
        for (i = 0; i < f_lev; i++)
            os << parent->refRatio(i)[0] << ' ';
        os << '\n';
        for (i = 0; i <= f_lev; i++)
            os << parent->Geom(i).Domain() << ' ';
        os << '\n';
        for (i = 0; i <= f_lev; i++)
            os << parent->levelSteps(i) << ' ';
        os << '\n';
        for (i = 0; i <= f_lev; i++) {
            for (int k = 0; k < BL_SPACEDIM; k++)
                os << parent->Geom(i).CellSize()[k] << ' ';
            os << '\n';
        }
        os << (int) gg->Coord() << '\n';
        os << "0\n"; // Write bndry data.

    }
    // Build the directory to hold the MultiFab at this level.
    // The name is relative to the directory containing the Header file.
    //
    static const std::string BaseName = "/Cell";
    char buf[64];
    sprintf(buf, "Level_%d", level);
    std::string Level = buf;
    //
    // Now for the full pathname of that directory.
    //
    std::string FullPath = dir;
    if (!FullPath.empty() && FullPath[FullPath.size() - 1] != '/')
        FullPath += '/';
    FullPath += Level;
    //
    // Only the I/O processor makes the directory if it doesn't already exist.
    //
    if (ParallelDescriptor::IOProcessor())
        if (!amrex::UtilCreateDirectory(FullPath, 0755))
            amrex::CreateDirectoryFailed(FullPath);
    //
    // Force other processors to wait till directory is built.
    //
    ParallelDescriptor::Barrier();

    if (ParallelDescriptor::IOProcessor()) {
        os << level << ' ' << grids.size() << ' ' << cur_time << '\n';
        os << parent->levelSteps(level) << '\n';

        for (i = 0; i < grids.size(); ++i) {
            RealBox gridloc = RealBox(grids[i], geom.CellSize(), geom.ProbLo());
            for (n = 0; n < BL_SPACEDIM; n++)
                os << gridloc.lo(n) << ' ' << gridloc.hi(n) << '\n';
        }
        //
        // The full relative pathname of the MultiFabs at this level.
        // The name is relative to the Header file containing this name.
        // It's the name that gets written into the Header.
        //
        if (n_data_items > 0) {
            std::string PathNameInHeader = Level;
            PathNameInHeader += BaseName;
            os << PathNameInHeader << '\n';
        }
    }
    //
    // We combine all of the multifabs -- state, derived, etc -- into one
    // multifab -- plotMF.
    // NOTE: In this tutorial code, there is no derived data
    int cnt = 0;
    const int nGrow = 0;
    MultiFab plotMF(grids, dmap, n_data_items, nGrow);
    MultiFab *this_dat = 0;
    //
    // Cull data from state variables -- use no ghost cells.
    //
    for (i = 0; i < plot_var_map.size(); i++) {
        int typ = plot_var_map[i].first;
        int comp = plot_var_map[i].second;
        this_dat = &state[typ].newData();
        MultiFab::Copy(plotMF, *this_dat, comp, cnt, 1, nGrow);
        cnt++;
    }
    //
    // Cull data from derived variables
    //   
    if (derive_names.size() > 0) {
        for (std::list<std::string>::iterator it = derive_names.begin();
             it != derive_names.end(); ++it) {
            auto derive_dat = derive(*it, cur_time, nGrow);
            MultiFab::Copy(plotMF, *derive_dat, 0, cnt, 1, nGrow);
            cnt++;
        }
    }

    //
    // Use the Full pathname when naming the MultiFab.
    //
    std::string TheFullPath = FullPath;
    TheFullPath += BaseName;
    VisMF::Write(plotMF, TheFullPath, how, true);

#ifdef PARTICLES
    if (do_tracers and level == 0) {
      TracerPC->Checkpoint(dir, "Tracer", true);
    }
#endif

}

