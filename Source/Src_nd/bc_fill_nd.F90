! license_AMReX.txt
module bc_fill_module

! since this is a .F90 file (instead of .f90) we run this through a C++ preprocessor
! for e.g., #if (BL_SPACEDIM == 1) statements.

  implicit none

  public

contains

  subroutine emfill(em,em_lo,em_hi,domlo,domhi,delta,xlo,time,bc) &
       bind(C, name="emfill")

    use amrex_fort_module, only : amrex_spacedim, amrex_real

    implicit none

    integer      :: em_lo(3),em_hi(3)
    integer      :: bc(amrex_spacedim,2)
    integer      :: domlo(3), domhi(3)
    real(amrex_real) :: delta(3), xlo(3), time
    real(amrex_real) :: em(em_lo(1):em_hi(1),em_lo(2):em_hi(2),em_lo(3):em_hi(3))

#if (BL_SPACEDIM == 1)
       call filcc(em,em_lo(1),em_hi(1),domlo,domhi,delta,xlo,bc)
#elif (BL_SPACEDIM == 2)
       call filcc(em,em_lo(1),em_lo(2),em_hi(1),em_hi(2),domlo,domhi,delta,xlo,bc)
#else
       call filcc(em,em_lo(1),em_lo(2),em_lo(3),em_hi(1),em_hi(2),em_hi(3),domlo,domhi,delta,xlo,bc)
#endif

  end subroutine emfill
  
end module bc_fill_module
