! license_AMReX.txt
module tagging_params_module

  double precision, save :: emerr(0:15), emgrad(0:15)

  integer, save :: max_emerr_lev, max_emgrad_lev

end module tagging_params_module
