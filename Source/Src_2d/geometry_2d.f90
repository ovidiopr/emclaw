! Created by ovidio on 11/10/20.

function RECTANGLE(x, pxlo, pxhi, y, pylo, pyhi) result(is_inside)
    double precision, intent(in) :: x, pxlo, pxhi, y, pylo, pyhi
    logical :: is_inside

    is_inside = ((y>=pylo).and.(y<=pyhi).and.(x>=pxlo).and.(x<=pxhi))
end function RECTANGLE

function CIRCLE(x, px, y, py, R) result(is_inside)
    double precision, intent(in) :: x, px, y, py, R
    logical :: is_inside

    is_inside = ((x - px)**2 + (y - py)**2 <= R**2)
end function CIRCLE

function ELLIPSE(x, px, y, py, a, b) result(is_inside)
    double precision, intent(in) :: x, px, y, py, a, b
    logical :: is_inside

    is_inside = (((x - px)**2/(a**2) + (y - py)**2/(b**2)) <= 1.0d0)
end function ELLIPSE
