! Created by ovidio on 11/10/20.

function BOX(x, pxlo, pxhi, y, pylo, pyhi, z, pzlo, pzhi) result(is_inside)
    double precision, intent(in) :: x, pxlo, pxhi, y, pylo, pyhi, z, pzlo, pzhi
    logical :: is_inside

    is_inside = ((z>=pzlo).and.(z<=pzhi).and.(y>=pylo).and.(y<=pyhi).and.(x>=pxlo).and.(x<=pxhi))
end function BOX

function SPHERE(x, px, y, py, z, pz, R) result(is_inside)
    double precision, intent(in) :: x, px, y, py, z, pz, R
    logical :: is_inside

    is_inside = ((x - px)**2 + (y - py)**2 + (z - pz)**2 <= R**2)
end function SPHERE

function ELLIPSOID(x, px, y, py, z, pz, a, b, c) result(is_inside)
    double precision, intent(in) :: x, px, y, py, z, pz, a, b, c
    logical :: is_inside

    is_inside = (((x - px)**2/(a**2) + (y - py)**2/(b**2) + (z - pz)**2/(c**2)) <= 1.0d0)
end function ELLIPSOID
