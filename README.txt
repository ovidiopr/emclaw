EMcLAW is a Maxwell's equation solver including AMR, polarization, perfect metals and
divergence control.

The whole code can be used just pasting it inside AMReX (amrex-master/Tutorials/Amr)
as if it was an extra tutorial.
